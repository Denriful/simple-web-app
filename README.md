# simple-web-app

Simple flask app

## HowTo run local

1. docker build -t web-app-demo:1.0 .
2. docker run -p 5000:5000 web-app-demo:1.0 

## HowTo run on GKE

1. kubectl apply -f kubernetes/namespace-dev.json
2. kubectl apply -f kubernetes/deployment.yaml --namespace=development
3. kubectl apply -f kubernetes/service.yaml --namespace=development
4. helm upgrade --install ingress-nginx ingress-nginx \                              
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
5. kubectl apply -f kubernetes/ingress-resource.yaml
6. helm install prometheus-blackbox prometheus-community/prometheus-blackbox-exporter --values values-blackbox.yml -n monitoring
7.  helm upgrade prometheus prometheus-community/kube-prometheus-stack --values values-prometheus.yml 

## HowTo run load generator

./load-test.sh 5000 http://34.118.14.15.nip.io/

## Alert in grafana
https://gitlab.com/Denriful/simple-web-app/-/blob/main/alert.png

Alert for 10% not implemented (only last max value)
